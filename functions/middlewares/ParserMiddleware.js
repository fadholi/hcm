const bodyParser = require('body-parser')
const Busboy = require('busboy');
const os = require('os')
const path = require('path');
const fs = require('fs');

module.exports = {
    body_parser: function(app){
        return function (req, _res, next) {
            if(req.get('content-type').indexOf('application/json') !== -1) {
                app.use(bodyParser.json());
                return next();
            } else if(req.get('content-type').indexOf('application/octet-stream') !== -1) {
                app.use(bodyParser.raw());
                return next();
            } else if(req.get('content-type').indexOf('text/plain') !== -1) {
                app.use(bodyParser.text());
                return next();
            } else if(req.get('content-type').indexOf('application/x-www-form-urlencoded') !== -1) {
                app.use(bodyParser.urlencoded());
                return next();
            } else {
                const busboy = new Busboy({headers: req.headers});
                busboy.on('field', (fieldname, val) => {
                    req.body[fieldname] = val;
                });
                busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
                    const filepath = path.join(os.tmpdir(), filename);
                    file.on('end', function() {
                        req.body[fieldname] = {
                            'filepath' : filepath,
                            'basefilename' : path.basename(filepath, path.extname(filepath)),
                            'extfilename' : filename.replace(path.basename(filepath, path.extname(filepath)),''),
                            'file' : file,
                            'filename' : filename,
                            'encodeing' : encoding,
                            'mimetype' : mimetype
                        }
                    });
                    file.pipe(fs.createWriteStream(filepath));
                });
                busboy.on('finish', function() {
                    return next();
                });
                busboy.end(req.body);
            }
        }
    }
}