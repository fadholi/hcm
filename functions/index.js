const functions = require('firebase-functions');
const express = require('express');
const cors = require('cors');
const axios = require('axios');
const ParserMiddleware = require('./middlewares/ParserMiddleware');
const FormData = require('form-data');
const fs =  require('fs');

axios.defaults.baseURL = 'https://asia-northeast1-core-services-2a547.cloudfunctions.net';
// axios.defaults.baseURL = 'http://172.16.2.128:5000/core-services-2a547/asia-northeast1';
axios.defaults.headers.common['Authorization'] = 'Bearer 71d3442a9bb48fa3b08660b48be43caa2e15e48f';

const app = express();
const whitelist = ['http://localhost:8080', 'https://job-portal-af68f.firebaseapp.com','https://jobs.gtalent.asia','https://gtalent.asia'];
app.use(cors());
// const configCors = {
//   origin: function (origin, callback) {
//     if(!origin) return callback(new Error('Access is Denieds.'), false);
//        if(whitelist.indexOf(origin) === -1) {
//              return callback(new Error('Access is Deniede.'), false);
//          }
//          return callback(null, true);
//   }
// }

const talent = app;
const area = app;
const jobs = app;

area.get('/province', (req, res) => {
    axios.get('provinsiList?page=1&limit=100').then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        console.log(error);
    });
});

area.get('/city', (req, res) => {
    let q=''
    if(req.query.provinceId !== undefined){
        q='&provinsi_id='+req.query.provinceId
    }
    axios.get('kotaList?page=1&limit=100'+q).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        console.log(error);
    });
});

area.get('/district', (req, res) => {
    let q=''
    if(req.query.cityId !== undefined){
        q='&kota_id='+req.query.cityId
    }
    axios.get('kecamatanList?page=1&limit=100'+q).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        console.log(error);
    });
});

area.get('/village', (req, res) => {
    let q=''
    if(req.query.districtId !== undefined){
        q='&kecamatan_id='+req.query.districtId
    }
    axios.get('kelurahanList?page=1&limit=100'+q).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        console.log(error);
    });
});

jobs.get('', (req, res) => {
    let q=''
    let page=''
    if(req.query.page !== undefined){
        q = '&page='+req.query.page;
    }
    axios.get('jobsList?limit=10'+q).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        console.log(error);
    });
});



jobs.get('/search', (req, res) => {
    let page = 0;
    let limit = 10;
    let q = '';
    let industry_id = '';
    let type = '';
    let salary = '';
    let salary_to = '';
    let location = '';
    if(req.query.page !== undefined && req.query.page > 0) {
        page = parseInt(req.query.page);
    }
    if(req.query.q !== undefined) {
        q = '&name='+req.query.q;
    }
    if(req.query.industry_id !== undefined) {
        industry_id = '&industry_id='+req.query.industry_id;
    }
    if(req.query.type !== undefined) {
        type = '&type='+req.query.type;
    }
    if(req.query.salary !== undefined) {
        salary = '&salary='+req.query.salary;
    }
    if(req.query.salary_to !== undefined) {
        salary_to = '&salary_to='+req.query.salary_to;
    }
    if(req.query.location !== undefined) {
        location = '&location='+req.query.location;
    }
    axios.get('jobsList?page='+page+'&limit='+limit+q+industry_id+type+salary+salary_to+location).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        console.log(error);
    });
});
jobs.get('/industry', (req, res) => {
    axios.get('industryAll').then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        console.log(error);
    });
});

jobs.get('/ListSearch', (req, res) => {
  axios.get('jobsListSearch').then(response => {
      return res.status(response.data.status).json(response.data.results).end();
  }).catch(error => {
      console.log(error);
  });
});

jobs.get('/detail/:id', (req, res) => {
  let q = "";
    if(req.query.talent_id !== undefined){
      q = "talent_id="+req.query.talent_id;
    }
    axios.get('jobsDetail/'+req.params.id+'?'+q).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        console.log(error);
    });
});

talent.post('/login', ParserMiddleware.body_parser(talent), (req, res) => {
    axios({
        method: 'post',
        url: 'userTalentLogin',
        data: {
            email: req.body.email,
            password: req.body.password,
        },
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        if(error.response.status===400){
            return res.status(400).json(error.response.data).end();
        }else{
            return res.status(500).send(error).end();
        }
    });
});

talent.post('/loginGoogle', ParserMiddleware.body_parser(talent), (req, res) => {
    axios({
        method: 'post',
        url: 'userTalentLoginByFirebase',
        data: {
            idToken: req.body.idToken,
            isWeb: req.body.isWeb,
            email: req.body.email
        },
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        if (error.response.status === 400) {
            return res.status(400).json(error.response.data).end();
        } else {
            return res.status(500).send(error).end();
        }
    });
  });


talent.post('/register', ParserMiddleware.body_parser(talent), (req, res) => {
    axios({
        method: 'post',
        url: 'userTalentRegister',
        data: {
            email: req.body.email,
            password: req.body.password,
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            phone: req.body.phone,
            referral: req.body.referral,
            disability:req.body.difabel
        },
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        return res.status(500).send(error).end();
    });
});

talent.post('/forgotPassword', ParserMiddleware.body_parser(talent), (req, res) => {
    axios({
        method: 'post',
        url: 'userTalentResetPassword',
        data: {
            email: req.body.email,
        },
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        return res.status(500).send(error).end();
    });
});

talent.get('/notif/unread/list/:id', (req, res) => {
    let q=''
    let page = 0
    let limit = 0
    page = '?page=' + req.query.page
    limit = '&limit=' + req.query.limit
    if (req.query.status !== undefined) {
        q='&status='+req.query.status
    }
    axios.get('notificationList/'+req.params.id+page+limit+q).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        console.log(error);
    });
});

talent.put('/notif/read/list/:id', (req, res) => {
    axios.put('readNotification/'+req.params.id).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        return res.status(500).send(error).end();
    });
});

talent.get('/cv/:id/:theme', (req, res) => {
  axios.get('generateCV/'+req.params.id+'/'+req.params.theme).then(response => {
      return res.status(response.data.status).json(response.data.results).end();
  }).catch(error => {
      console.log(error);
  });
});

talent.post('/uploadImage/:id', ParserMiddleware.body_parser(talent), (req, res) => {
    let formData = new FormData();
    formData.append('image', fs.createReadStream(req.body.image.filepath));
    axios.post('/talentPhoto/'+req.params.id,
    formData,
    {
        headers: formData.getHeaders()
    }).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        return res.status(500).send(error).end();
    });
});

talent.post('/apply/:id', ParserMiddleware.body_parser(talent), (req, res) => {
    axios({
        method: 'post',
        url: '/talentCreateJobsApplied/'+req.params.id,
        data:{
            jobs_id:req.body.id,
        },
        headers:{
            'Content-Type': 'application/json'
        }
    }).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        return res.status(500).send(error).end();
    });
});

talent.post('/List/:id', ParserMiddleware.body_parser(talent), (req, res) => {
    axios({
        method: 'post',
        url: '/talentListJobsApplied/'+req.params.id,
        data:{
            user_id:req.body.id_user,
        },
        headers:{
            'Content-Type': 'application/json'
        }
    }).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        return res.status(500).send(error).end();
    });
});

talent.put('/update/:id', ParserMiddleware.body_parser(talent), (req, res) => {
    axios({
        method: 'put',
        url: '/talentUpdate/'+req.params.id,
        data:{
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            gender:req.body.gender,
            marital:req.body.marital,
            nationality:req.body.nationality,
            birthplace: req.body.birthplace,
            birthdate:req.body.birthdate,
            headline:req.body.headline,
            summary:req.body.summary,
            id_number:req.body.id_number,
            disability:req.body.disability
        },
        headers:{
            'Content-Type': 'application/json'
        }
    }).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
      console.log(error)
        return res.status(500).send(error).end();
    });
});

talent.put('/update/contact/:id/:idc', ParserMiddleware.body_parser(talent), (req, res) => {
    axios({
        method: 'put',
        url: '/talentUpdateContact/'+req.params.id+'/'+req.params.idc,
        data:{
            type:req.body.type,
            name:req.body.name,
            value:req.body.value,
        },
        headers:{
            'Content-Type': 'application/json'
        }
    }).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        return res.status(500).send(error).end();
    });
});

talent.post('/update/address/:id', ParserMiddleware.body_parser(talent), (req, res) => {
    axios({
        method: 'post',
        url: '/talentCreateAddress/'+req.params.id,
        data:{
            kelurahan_id:req.body.kelurahan_id,
            address:req.body.address,
            type:"Domicile"
        },
        headers:{
            'Content-Type': 'application/json'
        }
    }).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        return res.status(500).send(error.response).end();
    });
});

talent.post('/skill/create/:id', ParserMiddleware.body_parser(talent), (req, res) => {
    let formData = new FormData();
    formData.append('skill',req.body.skill)
    formData.append('score',0)
    axios.post('/talentCreateSkill/'+req.params.id,
    formData,
    {
        headers: formData.getHeaders()
    }).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        return res.status(500).send(error.response).end();
    });
});

talent.get('/skill/list/:id', (req, res) => {
    axios.get('talentListSkill/'+req.params.id+'?page=1&limit=20').then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        console.log(error);
    });
})

talent.delete('/skill/remove/:id/:idS', (req, res) => {
    axios.delete('/talentDeleteSkill/'+req.params.id+'/'+req.params.idS).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        return res.status(500).send(error).end();
    });
});

talent.post('/update/currentAddress/:id/:idK', ParserMiddleware.body_parser(talent), (req, res) => {
    axios({
        method: 'put',
        url: '/talentUpdateAddress/'+req.params.id+'/'+req.params.idK,
        data:{
            kelurahan_id:req.body.kelurahan_id,
            address:req.body.address,
            type:"Domicile"
        },
        headers:{
            'Content-Type': 'application/json'
        }
    }).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        return res.status(500).send(error.response).end();
    });
});


talent.post('/bookmarked/save/:id', ParserMiddleware.body_parser(talent), (req, res) => {
    axios({
        method: 'post',
        url: '/talentCreateJobs/'+req.params.id,
        data:{
            jobs_id:req.body.jobs_id,
        },
        headers:{
            'Content-Type': 'application/json'
        }
    }).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        return res.status(500).send(error).end();
    });
});

talent.delete('/bookmarked/remove/:id/:idJ', (req, res) => {
    axios.delete('/talentDeleteJobs/'+req.params.id+'/'+req.params.idJ).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        return res.status(500).send(error).end();
    });
});

talent.post('/bookmarked/list/:id', ParserMiddleware.body_parser(talent), (req, res) => {
    axios({
        method: 'post',
        url: '/talentListJobs/'+req.params.id,
        data:{
            user_id:req.body.user_id,
        },
        headers:{
            'Content-Type': 'application/json'
        }
    }).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        return res.status(500).send(error).end();
    });
});

talent.post('/experience/create/:id', ParserMiddleware.body_parser(talent), (req, res) => {
    axios({
        method: 'post',
        url: '/talentCreateExperience/'+req.params.id,
        data:{
            company:req.body.company,
            location:req.body.location,
            position:req.body.position,
            start_month:req.body.start_month,
            start_year:req.body.start_year,
            finish_month:req.body.finish_month,
            finish_year:req.body.finish_year,
            description:req.body.description,
            employee_status:req.body.employee_status
        },
        headers:{
            'Content-Type': 'application/json'
        }
    }).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        console.log(error.response.data)
        return res.status(500).send(error).end();
    });
});

jobs.get('/experience/list/:id', (req, res) => {
    axios.get('talentListExperience/'+req.params.id).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        console.log(error);
    });
});
jobs.get('/experience/detail/:id/:idExp', (req, res) => {
    axios.get('talentDetailExperience/'+req.params.id+'/'+req.params.idExp).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        console.log(error);
    });
});

talent.put('/experience/update/:id/:idc', ParserMiddleware.body_parser(talent), (req, res) => {
    axios({
        method: 'put',
        url: '/talentUpdateExperience/'+req.params.id+'/'+req.params.idc,
        data:{
            company:req.body.company,
            location:req.body.location,
            position:req.body.position,
            start_month:req.body.start_month,
            start_year:req.body.start_year,
            finish_month:req.body.finish_month,
            finish_year:req.body.finish_year,
            description:req.body.description,
            employee_status:req.body.employee_status
        },
        headers:{
            'Content-Type': 'application/json'
        }
    }).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        return res.status(500).send(error).end();
    });
});

talent.delete('/experience/delete/:id/:idJ', (req, res) => {
    axios.delete('/talentDeleteExperience/'+req.params.id+'/'+req.params.idJ).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        return res.status(500).send(error).end();
    });
});

talent.post('/edu/create/:id', ParserMiddleware.body_parser(talent), (req, res) => {
    axios({
        method: 'post',
        url: '/talentCreateEducation/'+req.params.id,
        data:{
            school:req.body.school,
            degree:req.body.degree,
            study:req.body.study,
            start:req.body.start,
            finish:req.body.finish,
            description:req.body.description,
            gpa:req.body.gpa
        },
        headers:{
            'Content-Type': 'application/json'
        }
    }).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        return res.status(500).send(error).end();
    });
});

jobs.get('/edu/list/:id', (req, res) => {
    axios.get('talentListEducation/'+req.params.id).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        console.log(error);
    });
});

jobs.get('/edu/detail/:id/:idExp', (req, res) => {
    axios.get('talentDetailEducation/'+req.params.id+'/'+req.params.idExp).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        console.log(error);
    });
});

talent.put('/edu/update/:id/:idc', ParserMiddleware.body_parser(talent), (req, res) => {
    axios({
        method: 'put',
        url: '/talentUpdateEducation/'+req.params.id+'/'+req.params.idc,
        data:{
            school:req.body.school,
            degree:req.body.degree,
            study:req.body.study,
            start:req.body.start,
            finish:req.body.finish,
            description:req.body.description,
            gpa:req.body.gpa
        },
        headers:{
            'Content-Type': 'application/json'
        }
    }).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        return res.status(500).send(error).end();
    });
});

talent.delete('/edu/delete/:id/:idJ', (req, res) => {
    axios.delete('/talentDeleteEducation/'+req.params.id+'/'+req.params.idJ).then(response => {
        return res.status(response.data.status).json(response.data.results).end();
    }).catch(error => {
        return res.status(500).send(error).end();
    });
});

talent.post('/course/create/:id', ParserMiddleware.body_parser(talent), (req, res) => {
  axios({
      method: 'post',
      url: '/talentCreateCourse/'+req.params.id,
      data:{
          name:req.body.name,
          institution:req.body.institution,
          start_date:req.body.start_date,
          end_date:req.body.end_date
      },
      headers:{
          'Content-Type': 'application/json'
      }
  }).then(response => {
      return res.status(response.data.status).json(response.data.results).end();
  }).catch(error => {
      return res.status(500).send(error).end();
  });
});

talent.get('/course/list/:id', (req, res) => {
  axios.get('talentListCourse/'+req.params.id).then(response => {
      return res.status(response.data.status).json(response.data.results).end();
  }).catch(error => {
      console.log(error);
  });
});

talent.get('/course/detail/:id/:idExp', (req, res) => {
  axios.get('talentDetailCourse/'+req.params.id+'/'+req.params.idExp).then(response => {
      return res.status(response.data.status).json(response.data.results).end();
  }).catch(error => {
      console.log(error);
  });
});

talent.put('/course/update/:id/:idc', ParserMiddleware.body_parser(talent), (req, res) => {
  axios({
      method: 'put',
      url: '/talentUpdateCourse/'+req.params.id+'/'+req.params.idc,
      data:{
          name:req.body.name,
          institution:req.body.institution,
          start_date:req.body.start_date,
          end_date:req.body.end_date
      },
      headers:{
          'Content-Type': 'application/json'
      }
  }).then(response => {
      return res.status(response.data.status).json(response.data.results).end();
  }).catch(error => {
      return res.status(500).send(error).end();
  });
});

talent.delete('/course/delete/:id/:idJ', (req, res) => {
  axios.delete('/talentDeleteCourse/'+req.params.id+'/'+req.params.idJ).then(response => {
      return res.status(response.data.status).json(response.data.results).end();
  }).catch(error => {
      return res.status(500).send(error).end();
  });
});

talent.post('/organization/create/:id', ParserMiddleware.body_parser(talent), (req, res) => {
  axios({
      method: 'post',
      url: '/talentCreateOrganization/'+req.params.id,
      data:{
          name:req.body.name,
          description:req.body.description,
      },
      headers:{
          'Content-Type': 'application/json'
      }
  }).then(response => {
      return res.status(response.data.status).json(response.data.results).end();
  }).catch(error => {
      return res.status(500).send(error).end();
  });
});

talent.get('/organization/list/:id', (req, res) => {
  axios.get('talentListOrganization/'+req.params.id).then(response => {
      return res.status(response.data.status).json(response.data.results).end();
  }).catch(error => {
      console.log(error);
  });
});

talent.get('/organization/detail/:id/:idExp', (req, res) => {
  axios.get('talentDetailOrganization/'+req.params.id+'/'+req.params.idExp).then(response => {
      return res.status(response.data.status).json(response.data.results).end();
  }).catch(error => {
      console.log(error);
  });
});

talent.put('/organization/update/:id/:idc', ParserMiddleware.body_parser(talent), (req, res) => {
  axios({
      method: 'put',
      url: '/talentUpdateOrganization/'+req.params.id+'/'+req.params.idc,
      data:{
          name:req.body.name,
          description:req.body.description,
      },
      headers:{
          'Content-Type': 'application/json'
      }
  }).then(response => {
      return res.status(response.data.status).json(response.data.results).end();
  }).catch(error => {
      return res.status(500).send(error).end();
  });
});

talent.delete('/organization/delete/:id/:idJ', (req, res) => {
  axios.delete('/talentDeleteOrganization/'+req.params.id+'/'+req.params.idJ).then(response => {
      return res.status(response.data.status).json(response.data.results).end();
  }).catch(error => {
      return res.status(500).send(error).end();
  });
});

talent.post('/subscribe', ParserMiddleware.body_parser(talent), (req, res) =>{
  axios.defaults.baseURL = 'https://us3.api.mailchimp.com/3.0/lists/8c4b4bfab9/members';
  axios.defaults.headers.common['Authorization'] = 'Basic SW5mb0dUMTEyOjRkNmQ1N2Y2OTY1ZjYxNmJjMGM0ZTg4ODFhM2FjNTJhLXVzMw==';
  axios({
    method: 'post',
    data: {
      'email_address': req.body.email,
      'status': 'subscribed'
    },
    headers: {
      'Content-Type': 'application/json',
    }
  }).then(response => {
      return res.send(response.data);
  }).catch(error => {
      return res.status(500).send(error);
  });
});

talent.get('/assessment/list', (req, res) => {
  let q=''
    let page = 0
    let limit = 0
    page = '?page=' + req.query.page
    limit = '&limit=' + req.query.limit
    if (req.query.status !== undefined) {
        q='&status='+req.query.status
    }
  axios.get('/assesmentQuestionList'+page+limit+q).then(response => {
      return res.status(response.data.status).json(response.data.results).end();
  }).catch(error => {
      console.log(error);
  });
});

talent.post('/assessment/create', ParserMiddleware.body_parser(talent), (req, res) => {
  // axios.post('/assesmentResult', req.body)
  axios({
    method: 'post',
    url: '/assesmentResult',
    data: {
        'P': {
          'D': req.body.P.D,
          'I': req.body.P.I,
          'S': req.body.P.S,
          'C': req.body.P.C
        },
        'K': {
          'D': req.body.K.D,
          'I': req.body.K.I,
          'S': req.body.K.S,
          'C': req.body.K.C
        },
    },
    headers:{
        'Content-Type': 'application/json'
    }
  }).then(response => {
      return res.status(response.data.status).json(response.data.results).end();
  }).catch(error => {
      console.log(error);
  });
});

talent.post('/job/list', ParserMiddleware.body_parser(talent), (req, res) => {
  let q=''
    let page = 0
    let limit = 0
    let name = null
    page = '?page=' + (req.body.page ? req.body.page : '')
    limit = '&limit=' + (req.body.limit ? req.body.limit : '')
    name = '&name=' + (req.body.name ? req.body.name : '')
    console.log('/jobsList'+page+limit+name)
  axios.get('/jobsList'+page+limit+name).then(response => {
    return res.status(response.data.status).json(response.data.results).end();
  }).catch(error => {
      console.log(error);
  });
});




exports.area = functions.region('asia-northeast1').https.onRequest(area);
exports.talent = functions.region('asia-northeast1').https.onRequest(talent);
exports.jobs = functions.region('asia-northeast1').https.onRequest(jobs);
