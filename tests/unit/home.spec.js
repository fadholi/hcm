// import { expect } from 'chai'
import { shallowMount, mount } from '@vue/test-utils'
import Home from '@/views/Home.vue'


const expect = require('chai').expect

describe('Home.vue', () => {
  it('Define Home.vue', () => {
    expect(Home.name).to.be.a('string');
    expect(Home.name).to.equal('home');
  })
})
