/* eslint-disable */
import Vue from 'vue'
import Vuex from 'vuex'

// Modules import down here
import i18n from './modules/StoreI18n'
import StoreNotifications from '@/stores/modules/StoreNotifications'
import StoreLoading from '@/stores/modules/StoreLoading'
import StoreLogin from '@/stores/modules/StoreLogin'
import StoreEmployee from '@/stores/modules/StoreEmployee'
import StoreReimbursement from '@/stores/modules/StoreReimbursement'
import StoreFinancialServices from '@/stores/modules/StoreFinancialServices'
import StoreAttendance from '@/stores/modules/StoreAttendance'
import StoreDashboard from '@/stores/modules/StoreDashboard'
import StoreOrgchart from '@/stores/modules/StoreOrgchart'
import StoreSchedule from '@/stores/modules/StoreSchedule'
import StoreAlert from '@/stores/modules/StoreAlert'
import StoreTheme from '@/stores/modules/StoreTheme'


// Initialize vuex
Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    modules: {
        // Include imported modules in here
        i18n,
        StoreNotifications,
        StoreLoading,
        StoreLogin,
        StoreEmployee,
        StoreReimbursement,
        StoreFinancialServices,
        StoreAttendance,
        StoreDashboard,
        StoreOrgchart,
        StoreSchedule,
        StoreAlert,
        StoreTheme
    },
    strict: debug
})