/* eslint-disable no-shadow,consistent-return,no-param-reassign */
// import ApiReimbursement from '@/services/ApiReimbursement'
// import router from '@/routers'
// import ApiAttendance from '@/services/ApiAttendance'
import ApiDashboard from '@/services/ApiDashboard'

const state = {
  // Set default state(s) here
  // example: null,
  dashboard_state: {
    data: null
  }
}

const getters = {
  
}

const mutations = {

  // show list dashboard
  showListDashboard(state, payload) {
    state.dashboard_state.data = payload.data.data;
  },

}

const actions = {
  async listDashboard({commit, dispatch}) {
    try {
      const res = await ApiDashboard.listDashboard()
      commit('showListDashboard', res)
    } catch (error) {
      throw error
    }
  },

}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
