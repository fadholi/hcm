/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import ApiReimbursement from '@/services/ApiReimbursement'
import router from '@/routers'
import ApiAttendance from '@/services/ApiAttendance'

const state = {
    // Set default state(s) here
    // example: null,
    attendance_state: {
        shift: [],
        leave: [],
        approval: [],
        report: [],
        application: [],
    }
}

const getters = {

}

const mutations = {

    // save category
    saveCategory(state, payload) {
        state.reimbursement_state.add.push(payload)
    },

    // delete category
    deleteCategory(state, index) {
        state.reimbursement_state.add.splice(index, 1)
    },

    // edit category
    editCategory(state, payload) {

        state.reimbursement_state.add.splice(payload.index, 1, payload)
    },

    // show list
    showListShift(state, payload) {
        state.attendance_state.shift = payload.data.data;
    },

    // show list
    showListLeave(state, payload) {
        state.attendance_state.leave = payload.data.data;
    },

    showlistApprovalAttendance(state, payload) {
        state.attendance_state.approval = payload.data.data;
    },

    showListReportAttendance(state, payload) {
        state.attendance_state.report = payload.data.data;
    },

    showListApplication(state, payload) {
        state.attendance_state.application = payload.data.data;
    }

}

const actions = {
    async listShift({ commit, dispatch }) {
        try {
            const res = await ApiAttendance.listShift()
            commit('showListShift', res)
        } catch (error) {
            throw error
        }
    },

    async createShift({ commit, dispatch }, req) {
        try {
            const res = await ApiAttendance.createShift(req)
            await dispatch('listShift')
            dispatch('StoreAlert/showSuccess', { message: res.data.message }, { root: true })
        } catch (error) {
            dispatch('StoreAlert/showError', { message: error }, { root: true })
            throw error
        }
    },

    async editShift({ commit, dispatch }, req) {
        try {
            const res = await ApiAttendance.createShift(req)
            await dispatch('listShift')
            dispatch('StoreAlert/showSuccess', { message: res.data.message }, { root: true })
        } catch (error) {
            throw error
        }
    },

    async deleteShift({ commit, dispatch }, req) {
        try {
            const res = await ApiAttendance.deleteShift(req)
            await dispatch('listShift')
            dispatch('StoreAlert/showSuccess', { message: res.data.message }, { root: true })
        } catch (error) {
            throw error
        }
    },

    async listLeave({ commit, dispatch }) {
        try {
            const res = await ApiAttendance.listLeave()
            commit('showListLeave', res)
        } catch (error) {
            throw error
        }
    },

    async createLeave({ commit, dispatch }, req) {
        try {
            const res = await ApiAttendance.createLeave(req)
            await dispatch('listLeave')
            dispatch('StoreAlert/showSuccess', { message: res.data.message }, { root: true })
        } catch (error) {
            dispatch('StoreAlert/showError', { message: error.response.data.message }, { root: true })
            throw error
        }
    },

    async editLeave({ commit, dispatch }, req) {
        try {
            const res = await ApiAttendance.createLeave(req)
            await dispatch('listLeave')
            dispatch('StoreAlert/showSuccess', { message: res.data.message }, { root: true })
        } catch (error) {
            throw error
        }
    },

    async deleteLeave({ commit, dispatch }, req) {
        try {
            const res = await ApiAttendance.deleteLeave(req)
            await dispatch('listLeave')
            dispatch('StoreAlert/showSuccess', { message: res.data.message }, { root: true })
        } catch (error) {
            throw error
        }
    },


    async listApprovalAttendance({ commit, dispatch }, req) {
        try {
            const res = await ApiAttendance.listApprovalAttendance(req)
            commit('showlistApprovalAttendance', res)
        } catch (error) {
            throw error
        }
    },

    async listReportAttendance({ commit, dispatch }, req) {
        try {
            const res = await ApiAttendance.listReportAttendance(req)
            commit('showListReportAttendance', res)
        } catch (error) {
            throw error
        }
    },

    async listApplication({ commit, dispatch }, req) {
        try {
            const res = await ApiAttendance.listApplication(req)
            commit('showListApplication', res)
        } catch (error) {
            throw error
        }
    },

    // async deleteCategory({commit, dispatch}, req) {
    //   try {
    //     await ApiReimbursement.deleteCategory(req)
    //     await dispatch('listCategory')
    //     // commit('saveCategory', res)
    //   } catch (error) {
    //     throw error
    //   }
    // },

}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}