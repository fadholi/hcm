/* eslint-disable no-shadow,consistent-return,no-param-reassign */

const state = {
    
}

const getters = {
    
}

const mutations = {
    SAVE_THEME (state, payload) {
        localStorage.setItem('theme', payload)
    },
}

const actions = {
    updateTheme ({ commit }, theme) {
            commit('SAVE_THEME', theme)
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}
