/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import ApiReimbursement from '@/services/ApiReimbursement'
import router from '@/routers'

const state = {
    // Set default state(s) here
    // example: null,
    reimbursement_state: {
        add: []
    }
}

const getters = {

}

const mutations = {

    // save category
    saveCategory(state, payload) {
        state.reimbursement_state.add.push(payload)
    },

    // delete category
    deleteCategory(state, index) {
        state.reimbursement_state.add.splice(index, 1)
    },

    // edit category
    editCategory(state, payload) {

        state.reimbursement_state.add.splice(payload.index, 1, payload)
    },

    // show list
    showListCategory(state, payload) {
        state.reimbursement_state.add = payload.data.data;
    }

}

const actions = {
    async listCategory({ commit, dispatch }) {
        try {
            const res = await ApiReimbursement.listCategory()
            commit('showListCategory', res)
        } catch (error) {
            throw error
        }
    },

    async createCategory({ commit, dispatch }, req) {
        try {
            await ApiReimbursement.createCategory(req)
            await dispatch('listCategory')
            dispatch('StoreAlert/showSuccess', { message: 'Category created successfully !' }, { root: true })
                // commit('saveCategory', res)
        } catch (error) {
            throw error
        }
    },

    async deleteCategory({ commit, dispatch }, req) {
        try {
            await ApiReimbursement.deleteCategory(req)
            await dispatch('listCategory')
            dispatch('StoreAlert/showSuccess', { message: 'Category deleted successfully !' }, { root: true })
                // commit('saveCategory', res)
        } catch (error) {
            throw error
        }
    },

}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}