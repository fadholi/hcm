/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import ApiNotifications from '../../services/ApiNotifications'

const state = {
  // Set default state(s) here
  // example: null,
  notifications_unread_state: {
    list: [],
    total: 0,
    limit: 0,
    page: 0,
    length: 0
  },

  request_notifications_state: {
    page: 1,
    limit: 10
  }
}

const getters = {
  // Change value of state(s) with getter
  // example
  // testGetters () {
  //   return 'yup'
  // }
}

const mutations = {
  // example
  // testMutations (state, payload) {
  //   state.test = payload
  // },

  // store unread notification
  storeUnreadNotification (state, payload) {
    state.notifications_unread_state.list = payload.data.data
    state.notifications_unread_state.total = payload.data.total
    state.notifications_unread_state.limit = payload.data.limit
    state.notifications_unread_state.page = payload.data.page
    state.notifications_unread_state.length = payload.data.pageTotal
  }

  // // store request notification
  // storeRequestUnreadNotification (state, payload) {
  //   state.request_notifications_state.page = payload.page
  //   state.request_notifications_state.limit = payload.limit
  // },

  // resetRequestUnreadNotification (state) {
  //   state.request_notifications_state.page = 1
  //   state.request_notifications_state.limit = 10
  // }

}

const actions = {
  // example
  // async testActions ({ commit, dispatch }, req) {
  //   try {
  //     const res = await ApiNotifications.listNotification()
  //     commit('testMutations', res)
  //     dispatch('listUnreadNotification', res)
  //   } catch (err) {
  //     throw err
  //   }
  // },

  async listUnreadNotification ({ commit }, req) {
    commit('StoreLoading/showLoading', '', { root: true })
    try {
      const res = await ApiNotifications.listUnreadNotification(req)
      commit('storeUnreadNotification', res)
      commit('StoreLoading/hideLoading', '', { root: true })
    } catch (err) {
      commit('StoreLoading/hideLoading', '', { root: true })
      throw err
    }
  }

}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
