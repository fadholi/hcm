/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import ApiFinancialServices from '@/services/ApiFinancialServices'
import router from '@/routers'

const state = {
  // Set default state(s) here
  // example: null,
  financial_state: {
    add: []
  }
}

const getters = {
  
}

const mutations = {

  // save category
  saveCategory (state, payload) {
    state.reimbursement_state.add.push(payload)
  },

  // delete category
  deleteCategory (state, index) {
    state.reimbursement_state.add.splice(index, 1)
  },

  // edit category
  editCategory(state, payload) {

    state.reimbursement_state.add.splice(payload.index, 1, payload)
  },

  // show list
  showListFinancialServices(state, payload) {
    state.financial_state.add = payload.data.data;
  }

}

const actions = {
  async listFinancialServices({commit, dispatch}, req) {
    try {
      const res = await ApiFinancialServices.listFinancialServices(req)
      commit('showListFinancialServices', res)
    } catch (error) {
      throw error
    }
  },

  async createFinancialServices({commit, dispatch}, req) {
    try {
      await ApiFinancialServices.createFinancialServices(req)
      // await dispatch('listCategory')
      router.replace('/financial-services/history')
      // commit('saveCategory', res)
    } catch (error) {
      throw error
    }
  },

  async deleteCategory({commit, dispatch}, req) {
    try {
      await ApiReimbursement.deleteCategory(req)
      await dispatch('listCategory')
      // commit('saveCategory', res)
    } catch (error) {
      throw error
    }
  },

}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
