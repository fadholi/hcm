/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import ApiSchedule from '@/services/ApiSchedule'

const state = {
    // Set default state(s) here
    // example: null,
    schedule_state: {
        schedule: []
    }
}

const getters = {
    scheduleList(state) {
        return state.schedule_state.schedule.map((data) => {
            return {

                no: data.event_id,
                name: data.event_name,
                start_date: data.start_date,
                end_date: data.end_date

            }
        })
    },

}

const mutations = {

    // show list
    showList(state, payload) {
        state.schedule_state[payload.state] = payload.data.data;
    }

}

const actions = {
    async listSchedule({ commit, dispatch }) {
        try {
            const res = await ApiSchedule.listSchedule()
            res.state = 'schedule'
            commit('showList', res)
        } catch (error) {
            throw error
        }
    },

    async createSchedule({ commit, dispatch }, req) {
        try {
            const res = await ApiSchedule.createSchedule(req)
            dispatch('listSchedule')
            dispatch('StoreAlert/showSuccess', { message: 'Event created successfully !' }, { root: true })
        } catch (error) {
            throw error
        }
    },

    async deleteSchedule({ commit, dispatch }, req) {
        try {
            await ApiSchedule.deleteSchedule(req)
            dispatch('listSchedule')
            dispatch('StoreAlert/showSuccess', { message: 'Event deleted successfully !' }, { root: true })
        } catch (error) {
            throw error
        }
    },

}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}