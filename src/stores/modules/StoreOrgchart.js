/* eslint-disable no-shadow,consistent-return,no-param-reassign */
// import ApiReimbursement from '@/services/ApiReimbursement'
// import router from '@/routers'
// import ApiAttendance from '@/services/ApiAttendance'
import ApiDashboard from '@/services/ApiDashboard'
import ApiOrgchart from '@/services/ApiOrgchart';
import router from '@/routers';

const state = {
    // Set default state(s) here
    orgchart_state: {
        companies: [],
        divisions: [],
        departments: [],
        employee: [],
        current_company: null,
    }
}

const getters = {
    listDivisionGetters(state) {
        return [{ id: 0, name: 'None'}, ...state.orgchart_state.divisions]
    },
    listDepartmentGetters(state) {
        return [{ id: 0, name: 'None'}, ...state.orgchart_state.departments]
    }
}

const mutations = {

    // show list dashboard
    showList(state, payload) {
        state.orgchart_state[payload.state] = payload.data.data;
    },

}

const actions = {
    async listCompany({ commit, dispatch }) {
        try {
            const res = await ApiOrgchart.listCompany()
            res.state = 'companies';
            commit('showList', res)
        } catch (error) {
            throw error
        }
    },

    async listDivision({ commit, dispatch }, req) {
        try {
            const res = await ApiOrgchart.listDivision(req)
            res.state = 'divisions';
            commit('showList', res)
        } catch (error) {
            throw error
        }
    },

    async listDepartment({ commit, dispatch }, req) {
        try {
            const res = await ApiOrgchart.listDepartment(req)
            res.state = 'departments';
            commit('showList', res)
        } catch (error) {
            throw error
        }
    },

    async listEmployee({ commit, dispatch }, req) {
        try {
            const res = await ApiOrgchart.listEmployee(req)
            res.state = 'employee';
            commit('showList', res)
        } catch (error) {
            throw error
        }
    },

    async createCompany({ commit, dispatch }, req) {
        try {
            const res = await ApiOrgchart.createCompany(req)
                // res.state = 'departments';
            dispatch('listCompany')
        } catch (error) {
            throw error
        }
    },

    async createDivision({ commit, dispatch, state }, req) {
        try {
            const res = await ApiOrgchart.createDivision(req)
            dispatch('listDivision', router.currentRoute.params.id)
        } catch (error) {
            throw error
        }
    },

    async createDepartment({ commit, dispatch, }, req) {
        try {
            const res = await ApiOrgchart.createDepartment(req)
                // res.state = 'departments';
            dispatch('listDepartment', router.currentRoute.params.id)
        } catch (error) {
            throw error
        }
    },

    async deleteCompany({ commit, dispatch }, req) {
        try {
            const res = await ApiOrgchart.deleteCompany(req.id)
                // res.state = 'departments';
            dispatch('listCompany')
        } catch (error) {
            throw error
        }
    },



}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}
