/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import ApiNotifications from '../../services/ApiNotifications'

const state = {
    // Set default state(s) here
    // example: null,
    alert_state: {
        success: false,
        info: false,
        error: false,
        warning: false,

        success_message: null,
        info_message: null,
        error_message: null,
        warning_message: null,
    }
}

const getters = {}

const mutations = {
    // showSuccess(state, payload) {
    //     state.alert_state.success = true;
    //     state.alert_state.success_message = payload;
    //     setTimeout(() => {
    //         state.alert_state.success = false;
    //     }, 5000)
    // },

    // showInfo(state, payload) {
    //     state.alert_state.info = true;
    //     state.alert_state.info_message = payload;
    //     setTimeout(() => {
    //         state.alert_state.error = false;
    //     }, 5000)
    // },

    // showError(state, payload) {
    //     state.alert_state.error = true;
    //     state.alert_state.error_message = payload;
    //     setTimeout(() => {
    //         state.alert_state.error = false;
    //     }, 5000)
    // },

    // showWarning(state, payload) {
    //     state.alert_state.warning = true;
    //     state.alert_state.warning_message = payload;
    //     setTimeout(() => {
    //         state.alert_state.warning = false;
    //     }, 5000)
    // },

    show(state, payload) {
        state.alert_state[payload.state + '_message'] = payload.message;
        state.alert_state[payload.state] = true;
    },

    hide(state, payload) {
        state.alert_state[payload.state] = false;
    }
}

const actions = {
    showSuccess({ commit }, req) {
        req.state = 'success'
        commit('show', req)
        setTimeout(() => {
            commit('hide', req)
        }, 5000)
    },

    showInfo({ commit }, req) {
        req.state = 'info'
        commit('show', req)
        setTimeout(() => {
            commit('hide', req)
        }, 5000)
    },

    showError({ commit }, req) {
        req.state = 'error'
        commit('show', req)
        setTimeout(() => {
            commit('hide', req)
        }, 5000)
    },

    showInfo({ commit }, req) {
        req.state = 'warning'
        commit('show', req)
        setTimeout(() => {
            commit('hide', req)
        }, 5000)
    }

}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
};