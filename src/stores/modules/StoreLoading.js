/* eslint-disable no-shadow,consistent-return,no-param-reassign */

const state = {
  // Set default state(s) here
  // example: null,
  loading_state: {
    is_loading: true
  }
}

const getters = {
  // Change value of state(s) with getter
  // example
  // testGetters () {
  //   return 'yup'
  // }
}

const mutations = {
  // example
  // testMutations (state, payload) {
  //   state.test = payload
  // },

  // show Loading
  showLoading (state) {
    state.loading_state.is_loading = true
  },

  // show Loading
  hideLoading (state) {
    state.loading_state.is_loading = false
  }

}

const actions = {
  // example
  // async testActions ({ commit, dispatch }, req) {
  //   try {
  //     const res = await ApiNotifications.listNotification()
  //     commit('testMutations', res)
  //     dispatch('listUnreadNotification', res)
  //   } catch (err) {
  //     throw err
  //   }
  // },

}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
