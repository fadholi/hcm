/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import ApiLogin from '@/services/ApiLogin'
import router from '@/routers'

const state = {
  // Set default state(s) here
  // example: null,
  employee_state: {
    add_employee: null
  }
}

const getters = {
  
}

const mutations = {

  // save employee
  saveEmployee (state, payload) {
    state.employee_state.add_employee = payload
  },


}

const actions = {
  

}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
