/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import ApiLogin from '@/services/ApiLogin'
import router from '@/routers'

const state = {
    // Set default state(s) here
    // example: null,
    loading_state: {
        is_loading: true
    }
}

const getters = {
    // Change value of state(s) with getter
    // example
    // testGetters () {
    //   return 'yup'
    // }
}

const mutations = {
    // example
    // testMutations (state, payload) {
    //   state.test = payload
    // },

    // show Loading
    showLoading(state) {
        state.loading_state.is_loading = true
    },

    // show Loading
    showLoggedin(state, payload) {
        localStorage.setItem('token', payload.data.token)
        localStorage.setItem('user_id', payload.data.user_id)
        localStorage.setItem('user_name', payload.data.name)
        router.replace('/dashboard')
    }
}

const actions = {
    async login({ commit }, req) {
        try {
            const res = await ApiLogin.login(req)
            commit('showLoggedin', res)
        } catch (err) {
            throw err
        }
    },

}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}