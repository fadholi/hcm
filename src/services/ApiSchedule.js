import Api from './Api'

export default {
    listSchedule() {
        const api = Api.generateApi()
        return api.get('events')
            .then(res => res)
    },

    createSchedule(req) {
        const api = Api.generateApi()
        const bodyFormData = new FormData();
        bodyFormData.append('event_name', req.event_name);
        bodyFormData.append('start_date', req.start_date);
        bodyFormData.append('end_date', req.end_date);
        console.log(bodyFormData, 'dsadasd', req)
        return api.post('save_event', bodyFormData)
            .then(res => res)
    },

    deleteSchedule(req) {
        const api = Api.generateApi()
        return api.get('delete_data/tbl_event/event_id/' + req)
            .then(res => res)
    },
}