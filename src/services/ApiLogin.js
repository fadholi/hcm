import Api from './Api'

export default {
    login(req) {
        const api = Api.generateApiLogin()
        const bodyFormData = new FormData();
        bodyFormData.append('user_name', req.user_name);
        bodyFormData.append('password', req.password);
        return api.post('login', bodyFormData)
            .then(res => res)
    }
}