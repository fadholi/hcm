import Api from './Api'

export default {
    listShift() {
        const api = Api.generateApi()
        return api.get('shifts')
            .then(res => res)
    },

    createShift(req) {
        const api = Api.generateApi()
        const bodyFormData = new FormData();
        bodyFormData.append('shift_name', req.shift_name);
        bodyFormData.append('shift_start', req.shift_start);
        bodyFormData.append('shift_end', req.shift_end);
        return api.post('save_shift/' + (req.shift_id ? req.shift_id : ''), bodyFormData)
            .then(res => res)
    },

    deleteShift(req) {
        const api = Api.generateApi()
        return api.post('delete_data/tbl_shift/shift_id/' + req.shift_id)
            .then(res => res)
    },

    listLeave() {
        const api = Api.generateApi()
        return api.get('leave_category')
            .then(res => res)
    },

    createLeave(req) {
        const api = Api.generateApi()
        const bodyFormData = new FormData();
        bodyFormData.append('category', req.category);
        bodyFormData.append('total_leave', req.total_leave);
        return api.post('save_leave_category/' + (req.leave_category_id ? req.leave_category_id : ''), bodyFormData)
            .then(res => res)
    },

    deleteLeave(req) {
        const api = Api.generateApi()
        return api.post('delete_data/tbl_leave_category/leave_category_id/' + req.leave_category_id)
            .then(res => res)
    },

    listApprovalAttendance(req) {
        const api = Api.generateApi()
        const bodyFormData = new FormData();
        bodyFormData.append('department_id', req.department_id);
        bodyFormData.append('division_id', req.division_id);
        bodyFormData.append('company_id', req.company_id);
        bodyFormData.append('date', req.date);
        return api.post('attendance_approval_list', bodyFormData)
            .then(res => res)
    },

    listReportAttendance(req) {
        const api = Api.generateApi()
        const bodyFormData = new FormData();
        bodyFormData.append('department_id', req.department_id);
        bodyFormData.append('date', req.date);
        return api.post('attendance_report', bodyFormData)
            .then(res => res)
    },

    listApplication(req) {
        const api = Api.generateApi()
            // const bodyFormData = new FormData();
            // bodyFormData.append('department_id', req.department_id); 
            // bodyFormData.append('date', req.date); 
        return api.get('application_list')
            .then(res => res)
    },

    // deleteCategory (req) {
    //   const api = Api.generateApi()
    //   return api.delete('delete_data/tbl_expense_cat/'+req.expense_cat_id+'/'+req.company_id)
    //     .then(res => res)
    // }
}