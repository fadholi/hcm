import Api from './Api'

export default {
  listCategory () {
    const api = Api.generateApi()
    return api.get('reimbursement')
      .then(res => res)
  },

  createCategory (req) {
    const api = Api.generateApi()
    const bodyFormData = new FormData();
    bodyFormData.append('expense_cat', req.expense_cat); 
    bodyFormData.append('include_payroll', req.include_payroll); 
    return api.post('save_reimbursement', bodyFormData)
      .then(res => res)
  },

  deleteCategory (req) {
    const api = Api.generateApi()
    return api.post('delete_data/tbl_expense_cat/expense_cat_id/'+req.expense_cat_id)
      .then(res => res)
  }
}
