import Api from './Api'

export default {
  listDashboard () {
    const api = Api.generateApi()
    return api.get('dashboard')
      .then(res => res)
  },
}
