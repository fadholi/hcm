import Api from './Api'

export default {
    listFinancialServices(req) {
        const api = Api.generateApi()
        const bodyFormData = new FormData();
        bodyFormData.append('payment_start', req.payment_start);
        bodyFormData.append('payment_end', req.payment_end);
        return api.post('financial_services', bodyFormData)
            .then(res => res)
    },

    createFinancialServices(req) {
        const api = Api.generateApi()
        const bodyFormData = new FormData();
        bodyFormData.append('loan_for', req.loan_for);
        bodyFormData.append('transaction_code', req.transaction_code);
        bodyFormData.append('amount', req.amount);
        bodyFormData.append('installment', req.installment);
        bodyFormData.append('employee_id', req.employee_id);
        bodyFormData.append('company_contribution', req.company_contribution);
        bodyFormData.append('effective_date', req.effective_date);
        return api.post('save_financial_services', bodyFormData)
            .then(res => res)
    },

    deleteCategory(req) {
        const api = Api.generateApi()
        return api.delete('delete_data/tbl_expense_cat/' + req.expense_cat_id + '/' + req.company_id)
            .then(res => res)
    }
}