import axios from 'axios'

export default {
    generateApi() {
        console.log(localStorage.getItem('user_id'))
        const api = axios.create({
            baseURL: process.env.VUE_APP_BASE,
            headers: {
                // 'Content-Type': 'multipart/form-data',
                // 'X-Custom-Header': 'foobar',
                // 'Authorization': localStorage.getItem('user_id'),
                'User-id': localStorage.getItem('user_id')
            }
        })

        return api
    },
    generateApiLogin() {
        const api = axios.create({
            baseURL: process.env.VUE_APP_BASE,
            headers: {
                // 'Content-Type': 'multipart/form-data',
                // 'user-id': localStorage.getItem('user_id')
            }
        })

        return api
    }
}