import Api from './Api'

export default {
    listOrgchart() {
        const api = Api.generateApi()
        return api.get('orchart')
            .then(res => res)
    },

    listCompany() {
        const api = Api.generateApi()
        return api.get('company')
            .then(res => res)
    },

    listDivision(req) {
        const api = Api.generateApi()
        return api.get('division/?cid=' + req)
            .then(res => res)
    },

    createCompany(req) {
        const api = Api.generateApi()
        const bodyFormData = new FormData();
        bodyFormData.append('code', req.code);
        bodyFormData.append('name', req.name);
        bodyFormData.append('parent_id', req.parent_id ? req.parent_id : 0);
        return api.post('add_company', bodyFormData)
            .then(res => res)
    },

    createDivision(req) {
        const api = Api.generateApi()
        const bodyFormData = new FormData();
        bodyFormData.append('code', req.code);
        bodyFormData.append('name', req.name);
        bodyFormData.append('company_id', req.company_id);
        bodyFormData.append('parent_id', req.parent_id ? req.parent_id : 0);
        return api.post('add_division', bodyFormData)
            .then(res => res)
    },
    createDepartment(req) {
        const api = Api.generateApi()
        const bodyFormData = new FormData();
        bodyFormData.append('code', req.code);
        bodyFormData.append('name', req.name);
        bodyFormData.append('division_id', req.division_id);
        bodyFormData.append('parent_id', req.parent_id ? req.parent_id : 0);
        return api.post('add_department', bodyFormData)
            .then(res => res)
    },

    deleteCompany(req) {
        const api = Api.generateApi()
        return api.post('delete_data/company/id/' + req)
            .then(res => res)
    },

    listDepartment(req) {
        const api = Api.generateApi()
        return api.get('department/?did=' + req)
            .then(res => res)
    },

    listEmployee(req) {
        const api = Api.generateApi()
        return api.get('employee/?deptId=' + req)
            .then(res => res)
    },
}