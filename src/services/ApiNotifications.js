import Api from './Api'

export default {
  listUnreadNotification (req) {
    const api = Api.generateApi()
    const userID = JSON.parse(localStorage.getItem('user_id')) || 1569213836759
    const statusID = req.status ? '&status=0' : ''
    return api.get('talent/notif/unread/list/' + userID + '?page=' + req.page + '&limit=' + req.limit + statusID)
      .then(res => res)
  }

  // listNotification (req) {
  //   const apis = api.generateApi()
  //   return apis.post('category/list', req)
  //     .then(res => res)
  // },
}
