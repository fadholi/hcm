import en from './i18n/en'
import id from './i18n/id'

export default {
  en,
  id
}
