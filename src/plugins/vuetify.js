// src/plugins/vuetify.js

import Vue from 'vue'
import Vuetify from 'vuetify'
import store from '@/stores'
import '@fortawesome/fontawesome-free/css/all.css'
import id from 'vuetify/es5/locale/id'
import en from 'vuetify/es5/locale/en'

Vue.use(Vuetify)


const opts = {
    theme: {
        dark:false,
        options: {
          customProperties: true
        },
        themes: {
          light: {
            primary: '#DDAB27',
            secondary: '#424242',
            accent: '#82B1FF',
            error: '#FF5252',
            info: '#2196F3',
            success: '#4CAF50',
            warning: '#FFC107',
            red: '#DB5227',
            green: '#2ABC41',
            gold: '#DBAB27',
            lightenGold: '#EED592',
            white: '#FFFFFF',
            black: '#303030',
            sidebar: '#1D1B17',
            grey: '#8C8C8C',
            blackHeader: '#1D1B17',
          },
          dark: {
            primary: '#DBAB27',
            secondary: '#424242',
            accent: '#82B1FF',
            error: '#FF5252',
            info: '#2196F3',
            success: '#4CAF50',
            warning: '#FFC107',
            red: '#DB5227',
            green: '#2ABC41',
            gold: '#DBAB27',
            lightenGold: '#EED592',
            white: '#1D1B17',
            black: '#EEEEEE',
            sidebar: '#1D1B17',
            grey: '#EEEEEE',
            blackHeader: '#1D1B17',
          }
        }
      },
      icons: {
        iconfont: 'fa'
      },
      lang: {
        locales: { id , en },
        current: 'en'
      }
}

export default new Vuetify(opts)