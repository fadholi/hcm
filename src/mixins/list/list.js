import { mapState } from 'vuex';

export default {
    data() {
        return {
            maka: 'asdasdasd'
        }
    },
    computed: {
        ...mapState('StoreOrgchart', ['orgchart_state']),

        listDivision() {
            return this.orgchart_state.divisions
        },

        listDepartment() {
            return this.orgchart_state.departments
        },

        listCompany() {
            return this.orgchart_state.companies
        },

        listEmployee() {
            return this.orgchart_state.employee
        }

    }
}