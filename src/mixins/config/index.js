export default {
    data () {
        return {
            autoNumericOption:{
                digitGroupSeparator: '.',
                decimalCharacter: ',',
                decimalCharacterAlternative: '.',
                currencySymbol: '',
                currencySymbolPlacement: 'p',
                roundingMethod: 'U',
                minimumValue: '0'
            }
        }
    }
}