import Vue from 'vue'
import App from './App.vue'
import router from '@/routers'
import store from '@/stores'
import './registerServiceWorker'
import vuetify from '@/plugins/vuetify'
import DatePicker from 'vue2-datepicker';
import vSelect from 'vue-select'
import VueSweetalert2 from 'vue-sweetalert2';
import VueAutonumeric from 'vue-autonumeric/src/components/VueAutonumeric';
import VueCurrencyFilter from 'vue-currency-filter'
import { i18n } from './plugins/i18n.js'
import Waterfall from 'vue-waterfall-eric'
import VueTimepicker from 'vue2-timepicker'
import "animate.css"

import listData from '@/mixins/list/list'
import config from '@/mixins/config/index'


import 'vuetify/dist/vuetify.min.css'
import '@/sass/main.scss'
import 'vue2-timepicker/dist/VueTimepicker.css'
import 'vue2-datepicker/index.css';
import 'vue-select/dist/vue-select.css';
import 'sweetalert2/dist/sweetalert2.min.css';
import 'vue-orgchart/dist/style.min.css'

const options = {
    confirmButtonColor: '#DBAB27;',
    cancelButtonColor: '#ff7674',
};

Vue.config.productionTip = false

Vue.component('date-picker', DatePicker)
Vue.component('vue-timepicker', VueTimepicker)
Vue.component('vue-autonumeric', VueAutonumeric)
Vue.component('vue-select', vSelect)
Vue.use(VueSweetalert2, options);
Vue.use(Waterfall);
// Vue.use(VueAutonumeric);
Vue.use(require('vue-moment'));
Vue.use(VueCurrencyFilter, {
    symbol: '',
    thousandsSeparator: '.',
    fractionCount: 2,
    fractionSeparator: ',',
    symbolPosition: 'front',
    symbolSpacing: true
})

new Vue({
    i18n,
    router,
    mixins: [listData,config],
    store,
    vuetify,
    render: h => h(App)
}).$mount('#app')