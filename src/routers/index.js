import Vue from 'vue'
import Router from 'vue-router'
import store from '@/stores'

// import modules here
import RouteDashboard from './modules/RouteDashboard'
import RouteLogin from './modules/RouteLogin'
import RouteOrganization from './modules/RouteOrganization'
import RouteEmployee from './modules/RouteEmployee'
import RoutePayroll from './modules/RoutePayroll'
import RouteReimbursement from './modules/RouteReimbursement'
import RouteFinancialServices from './modules/RouteFinancialServices'
import RouteAttendance from './modules/RouteAttendance'
import RouteSchedule from './modules/RouteSchedule'
import RoutePeformanceReview from './modules/RoutePeformanceReview'
import RouteSetting from './modules/RouteSetting'
import RouteDashboardUser from './modules/RouteDashboardUser'

Vue.use(Router)

// register modules here
const moduleRoutes = [
    RouteDashboard,
    RouteLogin,
    RouteOrganization,
    RouteEmployee,
    RoutePayroll,
    RouteReimbursement,
    RouteFinancialServices,
    RouteAttendance,
    RouteSchedule,
    RoutePeformanceReview,
    RouteSetting,
    RouteDashboardUser
]

const baseRoutes = [{
    path: '*',
    redirect: '/login'
}]

const routes = baseRoutes.concat(...moduleRoutes)

const router = new Router({
    mode: 'history',
    routes,
    scrollBehavior(to, from, savedPosition) {
        return { x: 0, y: 0 }
    }
})

// validate route by middleware
function nextFactory(context, middleware, index) {
    const subsequentMiddleware = middleware[index];

    if (!subsequentMiddleware) return context.next;

    return () => {
        const nextMiddleware = nextFactory(context, middleware, index + 1);
        subsequentMiddleware({...context, next: nextMiddleware });
    };
}

router.beforeEach((to, from, next) => {
    if (to.meta.middleware) {
        const middleware = Array.isArray(to.meta.middleware) ?
            to.meta.middleware : [to.meta.middleware];

        const context = {
            from,
            next,
            router,
            to
        };

        const nextMiddleware = nextFactory(context, middleware, 1);
        return middleware[0]({...context, next: nextMiddleware });
    } else {
        next();
    }
});

export default router