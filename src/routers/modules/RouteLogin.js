import Login from '@/views/login/Login'
import auth from '@/routers/middlewares/auth'
import router from '@/routers'

export default [
    {
        path: '/login',
        name: 'login',
        component: Login,
        beforeEnter: (to, from, next) => {
          if (!localStorage.getItem('token')) {
            next();
          } else {
            router.replace("/dashboard");
          }
        },
        meta: {
          title: 'gtalent.asia | Login',
          layout: 'blank'
        }
      },
      {
        path: "/logout",
        beforeEnter: () => {
          localStorage.clear();
          router.replace("/login");
        }
      }
]