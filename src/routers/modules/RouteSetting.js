import SettingGeneral from '@/views/home/setting/SettingGeneral'
import SettingApproval from '@/views/home/setting/SettingApproval'
import SettingWorkingDay from '@/views/home/setting/SettingWorkingDay'
import Setting from '@/views/home/settings'
import GeneralSetting from '@/views/home/settings/general/general'
import PayrollSetting from '@/views/home/settings/feature/payroll'
import ReimbursementSetting from '@/views/home/settings/feature/ReimbursementSetting'
import ApprovalSetting from '@/views/home/settings/feature/ApprovalSetting'
import AttendanceSetting from '@/views/home/settings/feature/AttendanceSetting'
import ProfileSetting from '@/views/home/settings/account/profile'
import ChangePasswordSetting from '@/views/home/settings/account/changePassword'
import ThemeSetting from '@/views/home/settings/preference/theme'
import NotificationSetting from '@/views/home/settings/preference/notification'
import auth from '@/routers/middlewares/auth'

export default [{
        path: '/setting',
        name: 'Setting',
        component: Setting,
        meta: {
            title: 'gtalent.asia | Setting General',
            middleware: [auth]
        },
        children: [{
                path: '/setting/general',
                name: 'General Setting',
                component: GeneralSetting,
                meta: {
                    title: 'gtalent.asia | General Setting',
                    middleware: [auth]
                }
            },
            {
                path: '/setting/payroll',
                name: 'Payroll Setting',
                component: PayrollSetting,
                meta: {
                    title: 'gtalent.asia | Payroll Setting',
                    middleware: [auth]
                }
            },
            {
                path: '/setting/reimbursement',
                name: 'Reimbursement Setting',
                component: ReimbursementSetting,
                meta: {
                    title: 'gtalent.asia | Reimbursement Setting',
                    middleware: [auth]
                }
            },
            {
                path: '/setting/approval',
                name: 'Approval Setting',
                component: ApprovalSetting,
                meta: {
                    title: 'gtalent.asia | Approval Setting',
                    middleware: [auth]
                }
            },
            {
                path: '/setting/attendance',
                name: 'Attendance Setting',
                component: AttendanceSetting,
                meta: {
                    title: 'gtalent.asia | Attendance Setting',
                    middleware: [auth]
                }
            },
            {
                path: '/account/profile',
                name: 'Profile Setting',
                component: ProfileSetting,
                meta: {
                    title: 'gtalent.asia | Profile Setting',
                    middleware: [auth]
                }
            },
            {
                path: '/account/change-password',
                name: 'Change Password Setting',
                component: ChangePasswordSetting,
                meta: {
                    title: 'gtalent.asia | Change Password Setting',
                    middleware: [auth]
                }
            },
            {
                path: '/preference/theme',
                name: 'Theme Setting',
                component: ThemeSetting,
                meta: {
                    title: 'gtalent.asia | Theme Setting',
                    middleware: [auth]
                }
            },
            {
                path: '/preference/notification',
                name: 'Notification Setting',
                component: NotificationSetting,
                meta: {
                    title: 'gtalent.asia | Notification Setting',
                    middleware: [auth]
                }
            }
        ],
    },
    {
        path: '/setting/general',
        name: 'SettingGeneral',
        component: SettingGeneral,
        meta: {
            title: 'gtalent.asia | Setting General',
            middleware: [auth]
        }
    },
    {
        path: '/setting/approval',
        name: 'SettingApproval',
        component: SettingApproval,
        meta: {
            title: 'gtalent.asia | Setting Approval',
            middleware: [auth]
        }
    },
    {
        path: '/setting/working',
        name: 'SettingWorkingDay',
        component: SettingWorkingDay,
        meta: {
            title: 'gtalent.asia | Setting Working Day',
            middleware: [auth]
        }
    },
]