import Dashboard from '@/views/home/dashboard/Dashboard'
import auth from '@/routers/middlewares/auth'

export default [
    {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard,
        meta: {
          title: 'gtalent.asia | Dashboard',
          middleware: [auth]
        }
      },
]