import Schedule from '@/views/home/schedule/Schedule'
import auth from '@/routers/middlewares/auth'

export default [
    {
        path: '/schedule',
        name: 'Schedule',
        component: Schedule,
        meta: {
          title: 'gtalent.asia | Schedule',
          middleware: [auth]
        }
      },
]