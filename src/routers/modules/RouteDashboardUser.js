import DashboardUser from '@/views/new-home/dashboard/Dashboard'
import auth from '@/routers/middlewares/auth'

export default [

    {
        path: '/user/dashboard',
        name: 'dashboard',
        component: DashboardUser,
        meta: {
            title: 'gtalent.asia | Dashboard',
            middleware: [auth],
            layout: 'userDashboard'
        }
    }

]