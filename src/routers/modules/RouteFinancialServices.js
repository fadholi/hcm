import FinancialServices from '@/views/home/financial-services/FinancialServices'
import FinancialServicesHistory from '@/views/home/financial-services/FinancialServicesHistory'
import auth from '@/routers/middlewares/auth'

export default [
    {
        path: '/financial-services',
        name: 'financialServices',
        component: FinancialServices,
        meta: {
          title: 'gtalent.asia | Financial Services',
          middleware: [auth]
        }
    },
    {
        path: '/financial-services/history',
        name: 'financial Services History',
        component: FinancialServicesHistory,
        meta: {
          title: 'gtalent.asia | Financial Services History',
          middleware: [auth]
        }
    },
]