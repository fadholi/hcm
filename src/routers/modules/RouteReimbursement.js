import ReimbursementPayment from '@/views/home/reimbursement/ReimbursementPayment'
import ReimbursementHistory from '@/views/home/reimbursement/ReimbursementHistory'
import auth from '@/routers/middlewares/auth'

export default [
    {
      path: '/reimbursement/payment',
      name: 'reimbursementPayment',
      component: ReimbursementPayment,
      meta: {
        title: 'gtalent.asia | Reimbursement Payment',
        middleware: [auth]
      }
    },
    {
      path: '/reimbursement/history',
      name: 'reimbursementHistory',
      component: ReimbursementHistory,
      meta: {
        title: 'gtalent.asia | Reimbursement History',
        middleware: [auth]
      }
    },
]