import PayrollManage from '@/views/home/payroll/PayrollManage'
import PayrollEmployeeList from '@/views/home/payroll/PayrollEmployeeList'
import PayrollCreatePayment from '@/views/home/payroll/PayrollCreatePayment'
import PayrollPaymentHistory from '@/views/home/payroll/PayrollPaymentHistory'
import auth from '@/routers/middlewares/auth'

export default [
    {
        path: '/payroll/manage',
        name: 'PayrollManage',
        component: PayrollManage,
        meta: {
          title: 'gtalent.asia | Payroll Manage',
          middleware: [auth]
        }
    },
    {
        path: '/payroll/list',
        name: 'PayrollEmployeeList',
        component: PayrollEmployeeList,
        meta: {
          title: 'gtalent.asia | Payroll Employee List',
          middleware: [auth]
        }
    },
    {
        path: '/payroll/create',
        name: 'PayrollCreatePayment',
        component: PayrollCreatePayment,
        meta: {
          title: 'gtalent.asia | Payroll Create Payment',
          middleware: [auth]
        }
    },
    {
        path: '/payroll/history',
        name: 'PayrollPaymentHistory',
        component: PayrollPaymentHistory,
        meta: {
          title: 'gtalent.asia | Payroll History Payment',
          middleware: [auth]
        }
    }
]