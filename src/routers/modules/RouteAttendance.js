import AttendanceShift from '@/views/home/attendance/AttendanceShift'
import AttendanceApproval from '@/views/home/attendance/AttendanceApproval'
import AttendanceReport from '@/views/home/attendance/AttendanceReport'
import AttendanceApprovalOvertime from '@/views/home/attendance/AttendanceApprovalOvertime'
import AttendanceApplication from '@/views/home/attendance/AttendanceApplication'
import auth from '@/routers/middlewares/auth'

export default [
    {
        path: '/attendance/shift',
        name: 'attendanceShift',
        component: AttendanceShift,
        meta: {
          title: 'gtalent.asia | Attendance Shift',
          middleware: [auth]
        }
    },
  {
    path: '/attendance/approval',
    name: 'AttendanceApproval',
    component: AttendanceApproval,
    meta: {
      title: 'gtalent.asia | Attendance Approval',
      middleware: [auth]
    }
  },
  {
    path: '/attendance/application-list',
    name: 'AttendanceApplication',
    component: AttendanceApplication,
    meta: {
      title: 'gtalent.asia | Attendance Application',
      middleware: [auth]
    }
  },
  {
    path: '/attendance/approval-overtime',
    name: 'AttendanceApprovalOvertime',
    component: AttendanceApprovalOvertime,
    meta: {
      title: 'gtalent.asia | Attendance Approval Overtime',
      middleware: [auth]
    }
  },
  {
    path: '/attendance/report',
    name: 'AttendanceReport',
    component: AttendanceReport,
    meta: {
      title: 'gtalent.asia | Attendance Report',
      middleware: [auth]
    }
  },
]