import EmployeeAdd from '@/views/home/employee/EmployeeAdd'
import EmployeeList from '@/views/home/employee/EmployeeList'
import EmployeeUpdate from '@/views/home/employee/EmployeeUpdate'
import EmployeeDetail from '@/views/home/employee/EmployeeDetail'
import EmployeeReward from '@/views/home/employee/EmployeeReward'
import auth from '@/routers/middlewares/auth'

export default [
    {
        path: '/employee/add',
        name: 'EmployeeAdd',
        component: EmployeeAdd,
        meta: {
          title: 'gtalent.asia | Employee Add',
          middleware: [auth]
        }
    },
    {
        path: '/employee/list',
        name: 'EmployeeList',
        component: EmployeeList,
        meta: {
          title: 'gtalent.asia | Employee List',
          middleware: [auth]
        }
    },
    {
        path: '/employee/update/:id',
        name: 'EmployeeUpdate',
        props: true,
        component: EmployeeUpdate,
        meta: {
          title: 'gtalent.asia | Employee Update',
          middleware: [auth]
        }
    },
    {
      path: '/employee/detail/:id',
      name: 'EmployeeDetail',
      props: true,
      component: EmployeeDetail,
      meta: {
        title: 'gtalent.asia | Employee Detail',
        middleware: [auth]
      }
    },
    {
      path: '/employee/reward',
      name: 'EmployeeReward',
      component: EmployeeReward,
      meta: {
        title: 'gtalent.asia | Employee Reward',
        middleware: [auth]
      }
  }
]