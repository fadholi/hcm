import Dashboard from '@/views/home/peformance-review/peformance-review'
import auth from '@/routers/middlewares/auth'

export default [
    {
        path: '/peformance-review',
        name: 'performance-review',
        component: Dashboard,
        meta: {
          title: 'gtalent.asia | Peformance Review',
          middleware: [auth]
        }
      },
]