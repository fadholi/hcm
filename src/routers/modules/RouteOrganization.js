import OrganizationCompany from '@/views/home/organization/OrganizationCompany'
import OrganizationDivision from '@/views/home/organization/OrganizationDivision'
import OrganizationDepartment from '@/views/home/organization/OrganizationDepartment'
import OrganizationEmployee from '@/views/home/organization/OrganizationEmployee'
import auth from '@/routers/middlewares/auth'

export default [{
        path: '/organization/companies',
        name: 'OrganizationCompany',
        component: OrganizationCompany,
        meta: {
            title: 'gtalent.asia | Organization Company',
            middleware: [auth]
        }
    },
    {
        path: '/organization/division/:id',
        name: 'OrganizationDivision',
        component: OrganizationDivision,
        meta: {
            title: 'gtalent.asia | Organization Company',
            middleware: [auth]
        }
    },
    {
        path: '/organization/department/:id',
        name: 'OrganizationDepartment',
        component: OrganizationDepartment,
        meta: {
            title: 'gtalent.asia | Organization Company',
            middleware: [auth]
        }
    },

    {
        path: '/organization/employee/:id',
        name: 'OrganizationDepartment',
        component: OrganizationEmployee,
        meta: {
            title: 'gtalent.asia | Organization Company',
            middleware: [auth]
        }
    },
]