import router from "@/routers";

export default function auth({ next }) {
  if (!localStorage.getItem('token')) {
    return router.replace("/login");
  } else {
    return next();
  }
}
